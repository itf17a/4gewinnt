from copy import deepcopy
import time
class Game():
    def __init__(self):
        self.columnCount = 7
        self.rowCount = 6

        self.current_player = 0
        self.count_column = []
        self.field = []
        self.setup()

    def setup(self):
        self.field = [[0 for x in range(self.columnCount)] for x in range(self.rowCount)]
        self.count_column = [0 for x in range(self.columnCount)]
    
    def _printNewLine(self):
        """ Helper Method for printField, prints a new-line with corresponding headers,.. """
        line = "+"
        for i in range(0, self.columnCount):
            line += '-----+'
        print(line)

    def _getTranslation(self, content):

        if content == 0:
            return "   "
        elif content == 1:
            return " X "
        elif content == 2:
            return " O "

    def printField(self):
        """ Print the field human-readable in stdout """
        print("\nVIER GEWINNT\n")
        self._printNewLine()
        for row in reversed(self.field):
            print('| ', end='')
            for column in row:
                print(self._getTranslation(column) + ' | ', end='')
            print()
            self._printNewLine()
        
        print("Player" + self._getTranslation(self.current_player))
    
    def add_stone(self, col):
        c = int(col)

        row = self.count_column[c]
        column = self.columnCount

        if row < column:
            self.field[row][c] = self.current_player
            self.count_column[c] = row + 1

            if self.current_player == 1:
                self.current_player = 2
            else: self.current_player = 1

        else:
            print("fehler")

    def checkWin(self, in_row=4):
        in_row = in_row
        # Check horizontally

        for col in range(self.columnCount-(in_row-1)):
            for row in range(self.rowCount):
                if in_row==4:
                    if (self.field[row][col] == self.field[row][col+1] == self.field[row][col+2] == self.field[row][col+3] != 0):
                        return [self.field[row][col],"H" ]      

        # Check vertically
        for row in range(self.rowCount-(in_row-1)):
            for col in range(self.columnCount):
                if in_row == 4:
                    if (self.field[row][col] == self.field[row+1][col] == self.field[row+2][col] == self.field[row+3][col] != 0):
                        return [self.field[row][col],"V" ] 


        # Skip diagonal checks if column count is less than 4
        if (self.columnCount < in_row):
            return False


		# Check up-diagonally
        for col in range(self.columnCount-(in_row-1)):
            for row in range(self.rowCount-(in_row-1)):
                if in_row == 4:
                    if (self.field[row][col] == self.field[row+1][col+1] == self.field[row+2][col+2] == self.field[row+3][col+3] != 0):
                        return [self.field[row][col],"UD" ]
 

        # Check down-diagonally
        for col in range(in_row-1, self.columnCount):
            for row in range(self.rowCount-(in_row-1)):
                if in_row == 4:
                    if (self.field[row][col] == self.field[row+1][col-1] == self.field[row+2][col-2] == self.field[row+3][col-3] != 0):
                        return [self.field[row][col],"DD" ] 

        return False

    def check_next_round(self, runde=0):
        f = deepcopy(self.field)
        cc = deepcopy(self.count_column)
        current_player = deepcopy(self.current_player)
        
        feld = []
        for mein_feld in range(0,self.columnCount):
            feld.append(deepcopy(self.field))

        for i in range(0,len(feld)):
            self.field = feld[i]
            self.add_stone(i)
            #print(self.printField())
            #time.sleep(0.5)
            
            if self.checkWin() != False:
                return i

            if runde == 0:
                win = self.check_next_round(runde=runde+1)
                if win != None:
                    return win

            self.current_player = current_player
            self.count_column = deepcopy(cc)
            self.field = f
        
        self.current_player = current_player
        self.count_column = deepcopy(cc)
        self.field = f

        return None

"""
---------------------DatenBanken benutzung ------------------
h = ""
round = 0
for index, value in get enumerate(history):
    h += "and runde_%s == %s"%(index, value)
select_all = "select count(runde_%s) from Table where starter = True "%round + h +"group by runde_%s"round+1
select_win = "select count(runde_%s) from Table where starter = True "%round + h +"and win=True group by runde_%s"round+1"

col_max = None
w = 0.00
for index, value in enumerate(select_all):
    quote = select_win / select_all
    if w < quote:
        col_max = index

return col_max
"""

if "__main__" == __name__:
    a=Game()
    a.current_player = 1

    while True:
        a.printField()

        v = Game()
        v = deepcopy(a)
        ai = v.check_next_round()
        if ai:
            eingabe= ai
        else:
            eingabe = input()

        a.add_stone(eingabe)
        win = a.checkWin()


        if win != False:
            a.printField()
            print("Gewonnen hat: " + str(a.current_player))
            break;
