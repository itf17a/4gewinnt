from copy import deepcopy
import time
import random
class Game:
    def __init__(self, p, p2):
        self.__len_column = 7
        self.__len_rows = 6
        self.__max_stones = self.__len_column*self.__len_rows

        self.__player1 = p
        self.__player2 = p2

        self.__current_player = random.choice([p, p2])
        self.__started_player = self.__current_player
        self.__history = []

        self.__count_column = []
        self.__field = []
        self.__setup()

    def __setup(self):
        self.__field = [[0 for x in range(self.__len_column)] for x in range(self.__len_rows)]
        self.__count_column = [0 for x in range(self.__len_column)]

    def __printNewLine(self):
        """ Helper Method for printField, prints a new-line with corresponding headers,.. """
        line = "+"
        for i in range(0, self.__len_column):
            line += '-----+'
        print(line)

    def __getTranslation(self, content):

        if content == 0:
            return "   "
        elif content == self.__player1:
            return " X "
        elif content == self.__player2:
            return " O "

    def printField(self):
        """ Print the field human-readable in stdout """
        print("\nVIER GEWINNT\n")
        self.__printNewLine()
        for row in reversed(self.__field):
            print('| ', end='')
            for column in row:
                print(self.__getTranslation(column) + ' | ', end='')
            print()
            self.__printNewLine()

        print("Player" + self.__getTranslation(self.__current_player))

    def add_stone(self, col):
        c = int(col)
        try:
            row = self.__count_column[c]
        except:
            return "spalte gibt es nicht"

        if row < self.__len_rows:
            self.__field[row][c] = self.__current_player
            self.__count_column[c] = row + 1

            if self.__current_player == self.__player1:
                self.__current_player = self.__player2
            else:
                self.__current_player = self.__player1

            self.__history.append(row)
            return True
        else:
            return "spalte voll"

    def checkWin(self, in_row=4):
        if self.__max_stones == len(self.__history):
            return None
        # Check horizontally

        for col in range(self.__len_column-(in_row-1)):
            for row in range(self.__len_rows):
                if in_row==4:
                    if (self.__field[row][col] == self.__field[row][col+1] == self.__field[row][col+2] == self.__field[row][col+3] != 0):
                        return [self.__field[row][col],"horizontally" ]

        # Check vertically
        for row in range(self.__len_rows-(in_row-1)):
            for col in range(self.__len_column):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row+1][col] == self.__field[row+2][col] == self.__field[row+3][col] != 0):
                        return [self.__field[row][col],"vertically" ]


        # Skip diagonal checks if column count is less than 4
        if (self.__len_column < in_row):
            return False


		# Check up-diagonally
        for col in range(self.__len_column-(in_row-1)):
            for row in range(self.__len_rows-(in_row-1)):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row+1][col+1] == self.__field[row+2][col+2] == self.__field[row+3][col+3] != 0):
                        return [self.__field[row][col],"up-diagonally" ]
 

        # Check down-diagonally
        for col in range(in_row-1, self.__len_column):
            for row in range(self.__len_rows-(in_row-1)):
                if in_row == 4:
                    if (self.__field[row][col] == self.__field[row+1][col-1] == self.__field[row+2][col-2] == self.__field[row+3][col-3] != 0):
                        return [self.__field[row][col],"down-diagonally" ]

        return False

    def check_next_round(self, runde=0, befor=None):
        f = deepcopy(self.__field)
        cc = deepcopy(self.__count_column)
        current_player = deepcopy(self.__current_player)
        
        feld = []
        for mein_feld in range(0,self.__len_column):
            feld.append(deepcopy(self.__field))

        for i in range(0,len(feld)):
            self.__field = feld[i]
            if self.__len_rows-1 >= cc[i]:
                try:
                    self.add_stone(i)
                    #self.printField()
                    #time.sleep(0.1)
                except: return self.add_stone(i)
                #print(self.printField())
                #time.sleep(0.5)

                if self.checkWin() != False:
                    if befor == None or befor!=i:
                        return i

                if runde == 0:
                    win = self.check_next_round(runde=runde+1,befor=i)
                    if win != None:
                        return win

            # reset des spielfeldes
            self.__current_player = current_player
            self.__count_column = deepcopy(cc)
            self.__field = f

        # macht nichts kaputt bei normalen gebrauch unnötig
        self.__current_player = current_player
        self.__count_column = deepcopy(cc)
        self.__field = f

        return None

    @property
    def get_field(self):
        return self.__field

    @property
    def get_player1(self):
        return self.__player1

    @property
    def get_player2(self):
        return self.__player2

    @property
    def get_current_player(self):
        return self.__current_player

    @property
    def get_column_counter(self):
        return self.__count_column

    @property
    def get_history(self):
        return self.__history

    @property
    def get_started_player(self):
        return self.__started_player

    @property
    def get_copy(self):
        return deepcopy(self)

"""
---------------------DatenBanken benutzung ------------------
h = ""
round = 0
for index, value in get enumerate(history):
    h += "and runde_%s == %s"%(index, value)
select_all = "select count(runde_%s) from Table where starter = True "%round + h +"group by runde_%s"round+1
select_win = "select count(runde_%s) from Table where starter = True "%round + h +"and win=True group by runde_%s"round+1"

col_max = None
w = 0.00
for index, value in enumerate(select_all):
    quote = select_win / select_all
    if w < quote:
        col_max = index

return col_max
"""

class player:
    def __init__(self, name="player", ki=False):
        self.__name = name
        self.__id = random.randint(0, 1000000)
        self.__ki = False
        if ki == True:
            self.__ki = True

    def eingabe(self, game):
        if self.__ki == False:
            return input("Spalte: ")

        check=  game.check_next_round()
        if check != None:
            return check
        return random.randint(0,7)

    @property
    def get_id(self):
        return self.__id

    @property
    def get_name(self):
        return self.__name

    @property
    def get_ki(self):
        return self.__ki

if "__main__" == __name__:
    p1 = player(ki=True)
    p2 = player(ki=True)
    a = Game(p1.get_id, p2.get_id)
    while True:
        player_id = a.get_current_player
        if player_id == p1.get_id:
            eingabe = p1.eingabe(a.get_copy)
        else:
            eingabe = p2.eingabe(a.get_copy)

        a.add_stone(eingabe)
        win = a.checkWin()
        #time.sleep(0.2)
        a.printField()

        if win != False:
            if win == None:
                print("unentschieden")
                break;
            #a = Game(p1, p2)
            print("Gewonnen hat: " + str(a.get_current_player))
            break;
