-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Daten aus Tabelle 4gewinnt.turndata: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `turndata` DISABLE KEYS */;
INSERT INTO `turndata` (`entry`, `turn_1`, `turn_2`, `turn_3`, `turn_4`, `turn_5`, `turn_6`, `turn_7`, `turn_8`, `turn_9`, `turn_10`, `turn_11`, `turn_12`, `turn_13`, `turn_14`, `turn_15`, `turn_16`, `turn_17`, `turn_18`, `turn_19`, `turn_20`, `turn_21`, `turn_22`, `turn_23`, `turn_24`, `turn_25`, `turn_26`, `turn_27`, `turn_28`, `turn_29`, `turn_30`, `turn_31`, `turn_32`, `turn_33`, `turn_34`, `turn_35`, `turn_36`, `turn_37`, `turn_38`, `turn_39`, `turn_40`, `turn_41`, `turn_42`, `WIN`, `I_started`) VALUES
	
/*!40000 ALTER TABLE `turndata` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
