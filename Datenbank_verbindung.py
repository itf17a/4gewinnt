import sqlite3

class DB:

    def __init__(self, database='4gewinnt'):

        self.database = database
        self.display = False

        self.connect()

    def connect(self):

        self.connection = sqlite3.connect(self.database)
        self.cursor = self.connection.cursor()
        self.connected = True

    def close(self, data=None):
        """Close the SQLite3 database."""
        self.connection.commit()
        self.connection.close()
        self.connected = False

    def execute(self, statement):
        if not self.connected:
            #open a previously closed connection
            self.connect()
            #mark the connection to be closed once complete
        if type(statement) != str:
            return False

        try:
            self.cursor.execute(statement)
            data = self.cursor.fetchall()

        except sqlite3.Error as error:
            print ('An error occurred:', error.args[0])
            print ('For the statement:', statement)
            return False

        if not data:
            return False
        self.close()
        return data

if __name__ == '__main__':
	pass