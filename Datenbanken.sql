-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.37-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.5.0.5278
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportiere Datenbank Struktur für schule
CREATE DATABASE IF NOT EXISTS `schule` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `schule`;

-- Exportiere Struktur von Tabelle schule.artikel
CREATE TABLE IF NOT EXISTS `artikel` (
  `Art.-NR` int(10) unsigned NOT NULL,
  `Artikel` varchar(50) COLLATE utf8_bin NOT NULL,
  `Preis` double unsigned NOT NULL,
  `Sollbestand` int(10) unsigned NOT NULL,
  `Istbestand` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Art.-NR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Exportiere Daten aus Tabelle schule.artikel: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `artikel` DISABLE KEYS */;
/*!40000 ALTER TABLE `artikel` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle schule.bestellung
CREATE TABLE IF NOT EXISTS `bestellung` (
  `Bestellnummer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Bestelldatum` int(10) unsigned NOT NULL DEFAULT '0',
  `Lieferdatum` int(10) unsigned NOT NULL DEFAULT '0',
  `Artikelnummer` int(10) unsigned NOT NULL DEFAULT '0',
  `Kundennummer` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Bestellnummer`),
  KEY `Kundennummer` (`Kundennummer`),
  CONSTRAINT `Kundennummer` FOREIGN KEY (`Kundennummer`) REFERENCES `kundendaten` (`Kundennummer`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Exportiere Daten aus Tabelle schule.bestellung: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `bestellung` DISABLE KEYS */;
/*!40000 ALTER TABLE `bestellung` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle schule.kundendaten
CREATE TABLE IF NOT EXISTS `kundendaten` (
  `Kundennummer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vorname` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `Nachname` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `PLz` int(10) unsigned NOT NULL DEFAULT '0',
  `Ort` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `Straße` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `HNummer` int(10) unsigned NOT NULL DEFAULT '0',
  `HNZusatz` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`Kundennummer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Exportiere Daten aus Tabelle schule.kundendaten: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `kundendaten` DISABLE KEYS */;
/*!40000 ALTER TABLE `kundendaten` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle schule.stammdaten
CREATE TABLE IF NOT EXISTS `stammdaten` (
  `Name` varchar(50) COLLATE utf8_bin NOT NULL,
  `Postfach` int(11) NOT NULL,
  `Plz` int(11) NOT NULL,
  `Ort` varchar(50) COLLATE utf8_bin NOT NULL,
  `Straße` varchar(50) COLLATE utf8_bin NOT NULL,
  `Bank` varchar(50) COLLATE utf8_bin NOT NULL,
  `BLZ` int(11) NOT NULL,
  `KTnr.` int(11) NOT NULL,
  `BIC` varchar(50) COLLATE utf8_bin NOT NULL,
  `IBAN` varchar(50) COLLATE utf8_bin NOT NULL,
  `Kontoinhaber` varchar(50) COLLATE utf8_bin NOT NULL,
  `UStd. IdNr.` varchar(50) COLLATE utf8_bin NOT NULL,
  `Steuernummer` varchar(50) COLLATE utf8_bin NOT NULL,
  `AmtsGericht` varchar(50) COLLATE utf8_bin NOT NULL,
  `AmtsID` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Exportiere Daten aus Tabelle schule.stammdaten: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `stammdaten` DISABLE KEYS */;
/*!40000 ALTER TABLE `stammdaten` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
